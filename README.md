# WikiEducación. Prácticas y experiencias educativas

-   [PDF digital]
-   [PDF para impresión]
-   [PDF del forro]

Artículos:

-   [Wikipedia en la educación básica: percepciones de los docentes]
-   [Transedición (traducción y edición) de un artículo de Wikipedia como
    herramienta de formación de traductores]
-   [Wikieducación: repensar el conocimiento abierto en la era de la
    (des)información]
-   [Acercando Wikipedia a los bibliotecarios:derribando prejuicios y
    construyendo puentes]
-   [Wikipedia en la Universidad: Experiencias del Programa Leamos Wikipedia en
    el Aula]
-   [Archivos comunes]
-   [Wikipedia: un recurso para el conocimiento y divulgación de la
    biodiversidad en la Reserva de la Biosfera Sierra de Manantlán]
-   [Capital Natural en México y Wikipedia: construyendo la innovación social y
    educativa]
-   [Wikipedia: una herramienta útil durante la educación remota de emergencia
    para conocer la biodiversidad del invernadero de la ENP 5]
-   [Wikipedia y aprendizajes colaborativos en un laboratorio ciudadano]
-   [Wikipedia en el aula, una alternativa para la enseñanza de las artes
    visuales]

Los artículos también están disponibles en el directorio [`articles`] de este
repositorio.

## Producción

Requisitos: [Scribus], [Python], [QPDF] y [`pikepdf`].

Los pasos para producir estos archivos son:

1.  Edita `src/digital.sla` en [Scribus].
2.  Ejecuta `sh scripts/make.sh` para producir los PDF.

El script genera el archivo `src/print.sla` a partir del archivo
`src/digital.sla` para después exportar ambos SLA a PDF. Mientras se exportan,
[Scribus] se abrirá y cerrará automáticamente, solo ten paciencia.

## Licencia

Este libro está bajo licencia [Creative Commons BY-SA 3.0].

  [PDF digital]: https://gitlab.com/prolibreros/cc/wikieducacion2022/-/raw/no-masters/wikieducacion2022.digital.pdf
  [PDF para impresión]: https://gitlab.com/prolibreros/cc/wikieducacion2022/-/raw/no-masters/wikieducacion2022.print.pdf
  [PDF del forro]: https://gitlab.com/prolibreros/cc/wikieducacion2022/-/raw/no-masters/wikieducacion2022.jacket.pdf
  [Wikipedia en la educación básica: percepciones de los docentes]: https://doi.org/10.5281/zenodo.7370166
  [Transedición (traducción y edición) de un artículo de Wikipedia como herramienta de formación de traductores]:
    https://doi.org/10.5281/zenodo.7370180
  [Wikieducación: repensar el conocimiento abierto en la era de la (des)información]:
    https://doi.org/10.5281/zenodo.7370191
  [Acercando Wikipedia a los bibliotecarios:derribando prejuicios y construyendo puentes]:
    https://doi.org/10.5281/zenodo.7370198
  [Wikipedia en la Universidad: Experiencias del Programa Leamos Wikipedia en el Aula]:
    https://doi.org/10.5281/zenodo.7370212
  [Archivos comunes]: https://doi.org/10.5281/zenodo.7370232
  [Wikipedia: un recurso para el conocimiento y divulgación de la biodiversidad en la Reserva de la Biosfera Sierra de Manantlán]:
    https://doi.org/10.5281/zenodo.7370241
  [Capital Natural en México y Wikipedia: construyendo la innovación social y educativa]:
    https://doi.org/10.5281/zenodo.7370253
  [Wikipedia: una herramienta útil durante la educación remota de emergencia para conocer la biodiversidad del invernadero de la ENP 5]:
    https://doi.org/10.5281/zenodo.7370263
  [Wikipedia y aprendizajes colaborativos en un laboratorio ciudadano]: https://doi.org/10.5281/zenodo.7370271
  [Wikipedia en el aula, una alternativa para la enseñanza de las artes visuales]:
    https://doi.org/10.5281/zenodo.7370277
  [`articles`]: https://gitlab.com/prolibreros/cc/wikieducacion2022/-/tree/no-masters/articles
  [Scribus]: https://www.scribus.net
  [Python]: https://www.python.org
  [QPDF]: https://qpdf.sourceforge.io
  [`pikepdf`]: https://pikepdf.readthedocs.io
  [Creative Commons BY-SA 3.0]: https://creativecommons.org/licenses/by-sa/3.0/deed.es
