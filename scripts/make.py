#!/usr/bin/env python
# Producción de archivos PDF a partir del SLA para digital
# Basado en los ejemplos de script para CLI:
#   https://wiki.scribus.net/canvas/Command_line_scripts
# (c) 2022 Perro Tuerto <hi@perrotuerto.blog>
# Financiado por Wikimedia México <https://mx.wikimedia.org>
# Este código es software libre con licencia GPLv3
# scribus -g -ns -py scripts/make.py

import xml.etree.ElementTree as ET
from pathlib import Path
from pikepdf import Pdf, ObjectStreamMode


def main():
    name = "wikieducacion2022"
    root = (Path(__file__).parent / "..").resolve()
    sladigital = root / "src" / "digital.sla"
    slaprint = root / "src" / "print.sla"
    pdfdigital = root / f"{name}.digital.pdf"
    pdfprint = root / f"{name}.print.pdf"
    article = root / "articles" / name
    fix_digital(sladigital)
    make_print(sladigital, slaprint)
    export(sladigital, pdfdigital)
    export(slaprint, pdfprint, is_print=True)
    split(pdfdigital, article)


# 0. Diccionarios


def toc(key):
    contents = {
        "1.": "1. Agradecimientos",
        "2.": "2. Introducción",
        "3.": "3. Experiencias Wikieducativas",
        "Wikipedia en las artes,": "Wikipedia en las artes, la música y el patrimonio cultural",
        "Bibliotecas públicas y": "Bibliotecas públicas y Wikipedia",
        "Wikipedia en el aula": "Wikipedia en el aula. Fauna Silvestre de la Reserva de la biosfera Sierra de Manantlán",
        "Áreas Naturales Protegidas": "Áreas Naturales Protegidas en México",
        "LABNL: conocimiento": "LABNL: conocimiento en comunidad desde la Wikipedia",
        "Transedición de Wikipedia": "Transedición de Wikipedia en español",
        "Wikipedia como asignatura": "Wikipedia como asignatura en el plan de estudios universitario",
        "4.": "4. Prácticas pedagógicas en la construcción del conocimiento",
        "Wikipedia en la educación": "Wikipedia en la educación primaria: percepciones de los docentes",
        "Transedición \(traducción y": "Transedición (traducción y edición) de un artículo de Wikipedia como herramienta de formación de traductores",
        "Wikieducación: repensar el": "Wikieducación: repensar el conocimiento abierto en la era de la (des)información",
        "Acercando Wikipedia a los": "Acercando Wikipedia a los bibliotecarios: derribando prejuicios y construyendo puentes",
        "Archivos comunes": "Archivos comunes. Laboratorio de documentación en el aula con herramientas de Wikimedia",
        "Wikipedia: un recurso para": "Wikipedia: un recurso para el conocimiento y divulgación de la biodiversidad en la Reserva de la Biosfera Sierra de Manantlán. Una implementación piloto",
        "Capital Natural en México y Wikipedia: construyendo la": "Capital Natural en México y Wikipedia: construyendo la innovación social y educativa",
        "Wikipedia: una herramienta útil durante la educación": "Wikipedia: una herramienta útil durante la educación remota de emergencia para conocer la biodiversidad del invernadero de la ENP 5",
        "Wikipedia y aprendizajes": "Wikipedia y aprendizajes colaborativos en un laboratorio ciudadano",
        "Wikipedia en el aula,": "Wikipedia en el aula, una alternativa para la enseñanza de las artes visuales",
    }
    if key in contents:
        return contents[key]
    else:
        return None


def articles():
    return {
        # "Primera - última páginas": "Sufijo del apartado"
        "1-7": ".00.pre.pdf",
        "8-9": ".01.agradecimientos.pdf",
        "10-17": ".02.intro.pdf",
        "18-20": ".03.experiencias.pdf",
        "20-24": ".04.experiencia01.pdf",
        "24-26": ".05.experiencia02.pdf",
        "26-29": ".06.experiencia03.pdf",
        "30-32": ".07.experiencia04.pdf",
        "32-34": ".08.experiencia05.pdf",
        "34-38": ".09.experiencia06.pdf",
        "38-42": ".10.experiencia07.pdf",
        "42-46": ".11.experiencia08.pdf",
        "46-49": ".12.experiencia09.pdf",
        "50-52": ".13.experiencia10.pdf",
        "52-55": ".14.experiencia11.pdf",
        "56-58": ".15.practicas.pdf",
        "59-74": ".16.practica01.pdf",
        "75-97": ".17.practica02.pdf",
        "99-112": ".18.practica03.pdf",
        "113-132": ".19.practica04.pdf",
        "133-153": ".20.practica05.pdf",
        "155-172": ".21.practica06.pdf",
        "173-192": ".22.practica07.pdf",
        "193-206": ".23.practica08.pdf",
        "207-215": ".24.practica09.pdf",
        "217-236": ".25.practica10.pdf",
        "237-249": ".26.practica11.pdf",
    }


# 1. Arregla SLA para digital


def fix_digital(filename):
    with open(filename, "r") as file:
        tree = ET.parse(file)
    root = tree.getroot()
    for bookmark in root.iter("Bookmark"):
        title = bookmark.get("Title")
        if toc(title):
            bookmark.set("Text", toc(title))
    tree.write(filename, encoding="utf-8")


def make_print(infile, outfile):
    with open(infile, "r") as file:
        tree = ET.parse(file)
    root = tree.getroot()
    root = change_conf(root, "PDF")
    root = change_conf(root, "Printer")
    root = change_mode(root)
    tree.write(outfile, encoding="utf-8")


def change_conf(root, nodename):
    for node in root.iter(nodename):
        node = change_marks(node)
    return root


def change_marks(node):
    for a in ["useDocBleeds", "cropMarks", "bleedMarks", "registrationMarks"]:
        node.set(a, "1")
    return node


def change_mode(root):
    for pdf in root.iter("PDF"):
        pdf.set("RGBMode", "0")
        pdf.set("Grayscale", "1")
    return root


# 3. Exporta PDF para digital e impresión


def export(infile, outfile, is_print=False):
    # Scribus supone que las rutas son String
    scribus.openDoc(str(infile))
    out = scribus.PDFfile()
    out.file = str(outfile)
    out.save()
    optimize(outfile, is_print)


def optimize(filename, is_print):
    pdf = Pdf.open(filename, allow_overwriting_input=True)
    if is_print:
        del pdf.pages[0:2]
    for page in pdf.pages:
        page.remove_unreferenced_resources()
    pdf.save(
        linearize=True,
        object_stream_mode=ObjectStreamMode.generate,
        recompress_flate=True,
    )


# 4. Extrae artículos del PDF digital


def split(filename, path):
    pdf = Pdf.open(filename)
    for pp, ext in articles().items():
        pp = list(map(int, pp.split("-")))
        first = pp[0] - 1
        last = pp[1]
        article = Pdf.new()
        pages = pdf.pages[first:last]
        article.pages.extend(pages)
        article.save(path.with_suffix(ext), linearize=True)


# Ejecución
main()
